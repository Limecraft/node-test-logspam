FROM node:14.17.3

RUN groupadd --gid 24001 -r mojito \
  && useradd --uid 24001 -r -g mojito mojito

WORKDIR /src

COPY ./*.js /src/

COPY ./onembtext.txt /src/

COPY ./config /src/config

# Add package.json
COPY .npmrc package.json package-lock.json /src/

# Install app dependencies
RUN cd /src && npm ci

USER mojito

CMD [ "node", "index.js" ]

