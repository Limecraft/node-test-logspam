module.exports = {
    log: {
        file: '/tmp/testlogs/service.log'
    },
    tags: ['testing', 'logspam'],
    hasAccessToServerStorage: false,
    contentRoot: "/tmp",
    dataStorage: "/tmp",
    tempStorage: "/tmp"
};
