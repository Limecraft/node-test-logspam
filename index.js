'use strict';

let exitCode = 0;

const fs = require('fs-extra');
const { logger } = require('@limecraft/remote-services-core');

const sleep = (ms) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

const readData = (filePath) => {
    const prom = new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                logger.error(`Something went wrong while reading the file: ${err}`);
                reject(err);
                return;
            }

            resolve(data);
        });
    });

    return prom;
};

const check = async () => {
    try {
        logger.info('Starting');

        const data = await readData('./onembtext.txt');

        while (true) {
            await sleep(5000);
            logger.info(`${data}`);
        }
        logger.info('Ending');
    } catch (e) {
        logger.info(`checks failed: ${e}`);

        exitCode = 1;
    } finally {
        await end(); // Removing this makes it so nothing gets logged...

        logger.info('Finishing up.');

        // Finish succesfully.
        process.exit(exitCode);
    }
};

process.on('exit', (code) => {
    if (code !== 0) {
        const stack = new Error().stack;
        logger.info(`${stack}`);
    } else {
        logger.info('Exit received');
    }
});

process.on('SIGINT', () => {
    logger.info('SIGINT was called');
});

(async () => {await check()})();
